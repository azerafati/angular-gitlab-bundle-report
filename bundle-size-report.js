const appFolders = './dist/*'
const bundlePaths = '/@(*.js|*.css)'
const glob = require('glob')
const fs = require('fs-extra')

const reportFile = '.bundle-report.txt'

async function report() {
  fs.outputFileSync(reportFile, '')

  const gzip = await import('gzip-size')

  const report = {}
  const logBundleSize = (index, bundles, appName) => {
    report[appName] = {}

    bundles.forEach(bundle => {
      const bundleName = bundle.split('/').pop().split('.')[0]
      const gzipSize = gzip.gzipSizeFromFileSync(bundle)
      const { size } = fs.statSync(bundle)
      report[appName][bundleName] = { gzipSize: 0, size: 0 }
      report[appName][bundleName]['gzipSize'] = Math.ceil(gzipSize / 1024)
      report[appName][bundleName]['size'] = Math.ceil(size / 1024)
    })
  }

  const apps = glob.sync(appFolders)
  apps.forEach((app, index) => {
    const bundles = glob.sync(app + bundlePaths)
    logBundleSize(index, bundles, app.split('/').pop())
  })

  const mainReportFile = __dirname + '/.cache/' + 'bundle-size-report-main.json'
  if (process.env.GIT_BRANCH === 'main') {
    fs.outputJSONSync(mainReportFile, report)
    console.log('Write JSON report for main branch to >>>', mainReportFile)
  }
  if (!fs.pathExistsSync(mainReportFile)) {
    const msg =
      "🔆 Bundle report from `main` branch doesn't exist, please run the pipeline for `main` branch and try again!"
    fs.appendFileSync(reportFile, msg)
    return 0
  }

  const mainReport = fs.readJsonSync(mainReportFile)

  const difReport = {}
  Object.keys(report).forEach(app => {
    Object.keys(report[app]).forEach(bundle => {
      const mainReportSize = mainReport[app]?.[bundle]?.['size'] ?? 0
      const mainReportGzip = mainReport[app]?.[bundle]?.['gzipSize'] ?? 0
      const dif = mainReportSize - report[app][bundle]['size']
      if (dif) {
        if (!difReport[app]) {
          difReport[app] = {}
        }
        difReport[app][bundle] =
          mainReportSize + ' KB => ' + report[app][bundle]['size'] + ' KB ' + (dif > 0 ? '🟢' : '🔴')
        difReport[app][bundle] += '<br>' + mainReportGzip + ' gz => ' + report[app][bundle]['gzipSize'] + ' gz '
      }
    })
  })

  const affectedApps = Object.keys(difReport)

  async function generateTable() {
    let tablemark = await import('markdown-table')
    const bundles = [
      ...new Set(
        Object.values(difReport)
          .map(a => Object.keys(a))
          .flat()
      ),
    ]
    const table = tablemark.markdownTable([
      ['', ...affectedApps],
      ...bundles.map(bundle => [bundle, ...affectedApps.map(app => difReport[app][bundle] ?? '')]),
    ])
    fs.appendFileSync(reportFile, table)
  }

  if (!affectedApps.length) {
    const msg = '🔆 This merge request is not changing size of any app'
    fs.appendFileSync(reportFile, msg)
  } else {
    const msg = `🔆 Affected bundles by this merge request:
    `
    fs.appendFileSync(reportFile, msg)
    generateTable()
  }
}

report()
