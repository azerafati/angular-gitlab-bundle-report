# Angular bundle size tracking with Gitlab CI



## Why

For detailed information check out my article https://www.linkedin.com/pulse/angular-bundle-size-tracking-gitlab-ci-alireza-zerafati.


## How
Look at he `.bundle-size-report.js` for how the report is being generated. And to implement it in the pipeline use the sample script in the `.gitlab-ci.yml` file.



## Dependencies

Running this script of course requires NodeJS and NPM to be installed. Additionally, the script uses the following NPM packages:
* markdown-table  https://www.npmjs.com/package/markdown-table
* gzip-size  https://www.npmjs.com/package/gzip-size
